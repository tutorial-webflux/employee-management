# Employee Management
## About
Sample CRUD project of Spring WebFlux, Spring Security, and MongoDB with annotation-based endpoints.

- GET All Employees (/employee)
- GET Employee by Id (/employee/{id})
- GET Employee by Name (/employee/name/{name})
- POST Create Employee (/employee)
- PUT Update Employee (/employee/update/{id})
- DELETE Delete Product (/employee/delete/{id})

## Source
https://howtodoinjava.com/spring-webflux/spring-webflux-tutorial/