package com.tutorial.employeemanagement.service;

import com.tutorial.employeemanagement.dao.EmployeeRepository;
import com.tutorial.employeemanagement.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    public Flux<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Mono<Employee> findById(String id) {
        return employeeRepository.findById(id);
    }

    public Flux<Employee> findByName(String name) {
        return employeeRepository.findByName(name);
    }

    public Flux<Employee> findByNameLike(String name) {
        return employeeRepository.findByNameLike(name);
    }

    public Mono<Employee> create(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Mono<Employee> update(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Mono<Void> delete(String id) {
        return employeeRepository.deleteById(id);
    }
}
