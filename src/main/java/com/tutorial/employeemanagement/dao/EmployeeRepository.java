package com.tutorial.employeemanagement.dao;

import reactor.core.publisher.Flux;

import com.tutorial.employeemanagement.model.Employee;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface EmployeeRepository extends ReactiveMongoRepository<Employee, String> {
    @Query("{ 'name': ?0 }")
    Flux<Employee> findByName(String name);

    @Query("{ 'name': /.*?0.*/ }")
    Flux<Employee> findByNameLike(String name);

    // TODO: find salary less than

    // TODO: find salary more than
}
