package com.tutorial.employeemanagement.controller;

import com.tutorial.employeemanagement.model.Employee;
import com.tutorial.employeemanagement.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public Flux<Employee> findAll() {
        return employeeService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Employee>> findById(@PathVariable("id") String id) {
        return employeeService.findById(id)
            .map(employee -> new ResponseEntity<>(employee, HttpStatus.OK))
            .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/name/{name}")
    public Flux<Employee> findByName(@PathVariable("name") String name) {
        return employeeService.findByName(name);
    }

    @GetMapping("/name-like/{name}")
    public Flux<Employee> findByNameLike(@PathVariable("name") String name) {
        return employeeService.findByNameLike(name);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ResponseEntity<Employee>> create(@RequestBody Employee employee) {
        return employeeService.create(employee)
            .map(newEmployee -> new ResponseEntity<>(newEmployee, HttpStatus.CREATED))
            .defaultIfEmpty(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PutMapping("/update/{id}")
    public Mono<ResponseEntity<Employee>> update(@PathVariable("id") String id, @RequestBody Employee employee) {
        return employeeService.findById(id)
            .flatMap(employeeDb -> {
                employeeDb.setName(employee.getName());
                employeeDb.setSalary(employee.getSalary());
                return employeeService.create(employeeDb);
            })
            .map(updatedEmployee -> new ResponseEntity<>(updatedEmployee, HttpStatus.OK))
            .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<ResponseEntity<Employee>> delete(@PathVariable("id") String id) {
        return employeeService.findById(id)
            .flatMap(employee -> 
                employeeService.delete(id)
                .thenReturn(ResponseEntity.ok(employee))
            )
            .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}